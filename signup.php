
<!DOCTYPE HTML>
<html>
<head>
<title>User Registration Page</title>
<link href="teststyle.css" rel="stylesheet" type="text/css">
</head>
<body>

<form class="login" method="POST" action="user_registration.php">
    <h1 class="login-title">Create a New Account</h1>
	
	
	<div class="imgcontainer">
    <img src="avatar.jpg" alt="Avatar" class="avatar">
    </div>

	<input type="text" name="username" class="login-input" placeholder="Pick a user name" >
    <input type="text" name="fname" class="login-input" placeholder="First Name" autofocus >
    <input type="text" name="lname" class="login-input" placeholder="Last Name" >
	<input type="text" name="email" class="login-input" placeholder="Email Address" >
	<input type="password" name="password" class="login-input" placeholder="Password" >
	<input type="password" name="repassword" class="login-input" placeholder="Re-type Password" >
    <input type="submit" value="Sign Up" class="login-button">
	
<?php

	if (!isset($_GET['signup'])) {
			exit();
			}
	else {
		$signupCheck = $_GET['signup'];
		if ($signupCheck == "empty") {
				echo "<p align='center' style='color:red'>Please provide all the information!</p>";
				exit();
			}
		else if ($signupCheck == "email") {
			echo "<p align='center' style='color:red'> The email address you have provided is invalid.</p>";
			exit();
		}
		
		else if ($signupCheck == "emailunique") {
			echo "<p align='center' style='color:red'> Your email already appears to be in the record</p>";
			exit();
		}
		
		else if ($signupCheck == "userunique") {
			echo "<p align='center' style='color:red'> Username is already taken!</p>";
			exit();
		}
	}
?>
	
</form>
</body>
</html>



