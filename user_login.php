<?php
	require_once 'dbconfig.php';
	session_start();
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
		$username = $_POST['username'];
		$password = $_POST['password'];
		$hash = password_hash($password, PASSWORD_DEFAULT);
		
		
		if (empty($username) || empty($password)) {
			header("Location:test.php?signin=empty");
			exit();
		}
		
		else { 
			try {
				$conn = new PDO("mysql:host=$host; dbname=$dbname", $user, $pass);
	
				//set the PDO error mode to exception
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = "SELECT * FROM users
						WHERE username= :username";
						
						/* WHERE fname LIKE :fname OR
						lname LIKE :lname;'; */
					
				//prepare statement for execution
				$stmt = $conn->prepare($sql);
				$stmt->bindParam(":username", $username);
		
				//pass values to the query and execute it
				$stmt->execute();
				$count = $stmt->rowCount();
	
				
				
				if ($count == 1) {
					$result = $stmt->fetch(PDO::FETCH_ASSOC);
					$hash_pwd = $result['password'];
					$hash = password_verify($password, $hash_pwd);
					if($hash == false) {
						header("location: test.php?signin=invalid");
					}
					else {
						echo"<p>Welcome, user </p>";
					}
				}
				
				else {
					header("Location:test.php?signin=invalid");
				}
				
			}
			 catch (PDOException $pe) {
					die("Could not connect to the database $dbname :" .$pe->getMessage());
				}
		}
	}
			
?>