<?php
	require_once 'dbconfig.php';
?>


<?php
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		
		$username = $_POST["username"];
		$fname = $_POST["fname"];
		$lname = $_POST["lname"];
		$email = $_POST["email"];
		$password = $_POST["password"];
		$hash = password_hash($password, PASSWORD_DEFAULT);
		
		if (empty($username) || empty($fname) || empty($lname) || empty($email) || empty($password)) {
			header("Location:signup.php?signup=empty");
			exit();
		}
		
		else{
			$stmt = $conn->prepare("SELECT COUNT(username) AS userCount 
									FROM users WHERE username = :uniqueuser");								
			$stmt->execute(array('uniqueuser' => $username));
			$result=$stmt->fetch(PDO::FETCH_ASSOC);
			
			if ($result['userCount'] != 0) {
					header("Location:signup.php?signup=userunique");
				}
				
			else{
				$email = filter_var($email, FILTER_SANITIZE_EMAIL);
				
				//Validate email
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$stmt = $conn->prepare("SELECT COUNT(email) AS EmailCount 
										FROM users WHERE email = :uniqueemail");
				$stmt->execute(array('uniqueemail' => $email));
				#$stmt->bindParam(':uniqueemail', $email);
				$result=$stmt->fetch(PDO::FETCH_ASSOC);
				
					if ($result['EmailCount'] != 0) {
						header("Location:signup.php?signup=emailunique");
					}
			
					else {
						$sql = "INSERT INTO users (username, fname, lname, email, password)
								VALUES (:username, :fname, :lname, :email, :password);";
		
						$query = $conn->prepare($sql);
			
						//bind parameters to statement
						$query->bindParam(':username', $username);
						$query->bindParam(':fname', $fname);
						$query->bindParam(':lname', $lname);
						$query->bindParam(':email', $email);
						$query->bindParam(':password', $hash);
			
						$query->execute();
						echo "Records inserted successfully.";	
						unset($query);
					}
				}
					
				else {
					header("Location:signup.php?signup=email");
				}
			}
		}
	}
	
	else {
		header("Location:signup.php");
	}
	
?>