<?php

$host = 'localhost';
$dbname = 'user_management';
$user = 'root';
$pass = '';

try {
	$conn = new PDO("mysql:host=$host; dbname=$dbname", $user, $pass);
	//set the PDO error mode to exception
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
	}
catch (PDOException $pe) {
	die("Could not connect to the database $dbname :" .$pe->getMessage());
	}
?>